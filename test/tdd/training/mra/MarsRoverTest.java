package tdd.training.mra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

public class MarsRoverTest {

	@Test
	public void testVerifyObstacleAtCellWith4XAnd7Y() throws MarsRoverException {
		int planetX = 10;
		int planetY = 10;
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("4,7");
		planetObstacles.add("2,3");
		MarsRover marsRover = new MarsRover(planetX, planetY, planetObstacles);
		assertTrue(marsRover.planetContainsObstacleAt(4, 7));
	}
	
	@Test
	public void testVerifyObstacleAtCellWith2XAnd3Y() throws MarsRoverException {
		int planetX = 10;
		int planetY = 10;
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("4,7");
		planetObstacles.add("2,3");
		MarsRover marsRover = new MarsRover(planetX, planetY, planetObstacles);
		assertTrue(marsRover.planetContainsObstacleAt(2, 3));
	}
	
	@Test
	public void testLanding() throws MarsRoverException {
		int planetX = 10;
		int planetY = 10;
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("4,7");
		planetObstacles.add("2,3");
		MarsRover marsRover = new MarsRover(planetX, planetY, planetObstacles);
		String commandString = "";
		assertEquals("0,0,N", marsRover.executeCommand(commandString));
	}
	
	@Test
	public void testTurningRight() throws MarsRoverException {
		int planetX = 10;
		int planetY = 10;
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("4,7");
		planetObstacles.add("2,3");
		MarsRover marsRover = new MarsRover(planetX, planetY, planetObstacles);
		String commandString = "r";
		assertEquals("0,0,E", marsRover.executeCommand(commandString));
	}

	@Test
	public void testTurningLeft() throws MarsRoverException {
		int planetX = 10;
		int planetY = 10;
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("4,7");
		planetObstacles.add("2,3");
		MarsRover marsRover = new MarsRover(planetX, planetY, planetObstacles);
		String commandString = "l";
		assertEquals("0,0,W", marsRover.executeCommand(commandString));
	}
	
	@Test
	public void testMovingForward() throws MarsRoverException {
		int planetX = 10;
		int planetY = 10;
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("4,7");
		planetObstacles.add("2,3");
		MarsRover marsRover = new MarsRover(planetX, planetY, planetObstacles);
		MarsRover.setRoverPosition(7,6,"N");
		String commandString = "f";
		assertEquals("7,7,N", marsRover.executeCommand(commandString));
	}
	
	@Test
	public void testMovingBackward() throws MarsRoverException {
		int planetX = 10;
		int planetY = 10;
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("4,7");
		planetObstacles.add("2,3");
		MarsRover marsRover = new MarsRover(planetX, planetY, planetObstacles);
		MarsRover.setRoverPosition(5,8,"E");
		String commandString = "b";
		assertEquals("4,8,E", marsRover.executeCommand(commandString));
	}
	
	@Test
	public void testMovingCombined() throws MarsRoverException {
		int planetX = 10;
		int planetY = 10;
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("4,7");
		planetObstacles.add("2,3");
		MarsRover marsRover = new MarsRover(planetX, planetY, planetObstacles);
		MarsRover.setRoverPosition(0,0,"N");
		String commandString = "ffrff";
		assertEquals("2,2,E", marsRover.executeCommand(commandString));
	}
	
	@Test
	public void testWrapping() throws MarsRoverException {
		int planetX = 10;
		int planetY = 10;
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("4,7");
		planetObstacles.add("2,3");
		MarsRover marsRover = new MarsRover(planetX, planetY, planetObstacles);
		MarsRover.setRoverPosition(0,0,"N");
		String commandString = "b";
		assertEquals("0,9,N", marsRover.executeCommand(commandString));
	}
	
	@Test
	public void testSingleObstacle() throws MarsRoverException {
		int planetX = 10;
		int planetY = 10;
		List<String> planetObstacles = new ArrayList<>();
		planetObstacles.add("2,2");
		MarsRover marsRover = new MarsRover(planetX, planetY, planetObstacles);
		MarsRover.setRoverPosition(0,0,"N");
		String commandString = "ffrfff";
		assertEquals("1,2,E,2,2", marsRover.executeCommand(commandString));
	}	
}
