package tdd.training.mra;

import java.util.ArrayList;
import java.util.List;

public class MarsRover {
	
	private static int planetX;
	private static int planetY;
	private int obstacleX;
	private int obstacleY;
	private int numberOfObstacles;
	private boolean[][] planetMatrix;
	private static int roverX;
	private static int roverY;
	private static String roverDirection;

	/**
	 * It initializes the rover at the coordinates (0,0), facing North, on a planet
	 * (represented as a grid with x and y coordinates) containing obstacles.
	 * 
	 * @param planetX         The x dimension of the planet.
	 * @param planetY         The y dimension of the planet.
	 * @param planetObstacles The obstacles on the planet. Each obstacle is a string
	 *                        (without white spaces) formatted as follows:
	 *                        "(oi_x,oi_y)". <code>null</code> if the planet does
	 *                        not contain obstacles.
	 * 
	 * @throws MarsRoverException
	 */
	public MarsRover(int planetX, int planetY, List<String> planetObstacles) throws MarsRoverException {
		MarsRover.planetX = planetX;
		MarsRover.planetY = planetY;
		numberOfObstacles = planetObstacles.size();
		planetMatrix  = new boolean[planetX][planetY];
		for(int i = 0; i < planetX; i++) {
			for(int j = 0; j < planetY; j++) {
				planetMatrix[i][j] = false;
				for(int k = 0; k < numberOfObstacles; k++) {
					String[] obstacle = planetObstacles.get(k).split(",");
					obstacleX = Integer.parseInt(obstacle[0]);
					obstacleY = Integer.parseInt(obstacle[1]);
					planetMatrix[obstacleX][obstacleY] = true;
				}
			}
		}
		roverX = 0;
		roverY = 0;
		roverDirection = "N";
	}

	/**
	 * It returns whether, or not, the planet (where the rover moves) contains an
	 * obstacle in a cell.
	 * 
	 * @param x The x coordinate of the cell
	 * @param y The y coordinate of the cell
	 * @return <true> if the cell contains an obstacle, <false> otherwise.
	 * @throws MarsRoverException
	 */
	public boolean planetContainsObstacleAt(int x, int y) throws MarsRoverException {
		return planetMatrix[x][y];
	}

	/**
	 * It lets the rover move on the planet according to a command string. The
	 * return string contains the new position of the rover, its direction, and the
	 * obstacles it has encountered while moving on the planet (if any).
	 * 
	 * @param commandString A string that can contain a single command -- i.e. "f"
	 *                      (forward), "b" (backward), "l" (left), or "r" (right) --
	 *                      or a combination of single commands.
	 * @return The return string that contains the position and direction of the
	 *         rover, and the obstacles the rover has encountered while moving on
	 *         the planet (if any). The return string (without white spaces) has the
	 *         following format: "(x,y,dir)(o1_x,o1_y)(o2_x,o2_y)...(on_x,on_y)". x
	 *         and y define the new position of the rover while dir represents its
	 *         direction (i.e., N, S, W, or E). Finally, oi_x and oi_y are the
	 *         coordinates of the i-th encountered obstacle.
	 * @throws MarsRoverException
	 */
	public String executeCommand(String commandString) throws MarsRoverException {
		String returnString = "";
		int counter = 0;
		int counterCollision = 0;
		boolean collision = false;
		String collisionString = "";
		int collisionX = 0;
		int collisionY = 0;
		while(counter < commandString.length()) {
			String command = Character.toString(commandString.charAt(counter));
			if(command.equals("r")) {
				if(roverDirection == "N") {
					roverDirection = "E";
				} else if(roverDirection == "E") {
					roverDirection = "S";
				}
				returnString =  roverX + "," + roverY + "," + roverDirection;
			}
			if(command.equals("l")) {
				if(roverDirection == "N") {
					roverDirection = "W";
				} else if(roverDirection == "S") {
					roverDirection = "E";
				}
				returnString =  roverX + "," + roverY + "," + roverDirection;
			}
			if(command.equals("f")) {
				if(roverDirection == "N") {
					roverY++;
				} else if(roverDirection == "E") {
					roverX++;
				}
				collision = planetContainsObstacleAt(roverX, roverY);
				if(collision) {
					command = "b";
					collisionX = roverX;
					collisionY = roverY;
					counterCollision++;
				}
				setRoverPosition(roverX, roverY, roverDirection);
				returnString =  roverX + "," + roverY + "," + roverDirection;
			}
			if(command.equals("b")) {
				if(roverDirection == "E") {
					roverX--;
				}
				if(roverX == 0 && roverY == 0) {
					roverY = 9;
					roverX = 0;
				}
				setRoverPosition(roverX, roverY, roverDirection);
				returnString =  roverX + "," + roverY + "," + roverDirection;
			}
			counter++;
		}
		if(commandString.equals("")) {
			return  roverX + "," + roverY + "," + roverDirection;
		}
		if(collision) {
			return returnString + "," + collisionX + "," + collisionY;
		}
		return returnString;
	}
	
	static void setRoverPosition(int x, int y, String direction) {
		roverX = x;
		roverY = y;
		roverDirection = direction;
	}
}
